renderDSSV = function (dssv) {
  var contentHTML = "";

  dssv.forEach((SV) => {
    var contentTr = `


   <tr>
   <td>${SV.id}</td>
   <td>${SV.name}</td>
   <td>${SV.email}</td>
   <td>${(SV.math+SV.physics+SV.chemistry)/3}</td>
   <td>
   <button 
   onclick=xoaSinhVien('${SV.id}')
   class="btn btn-danger">Xoá</button>
   <button 
   onclick="suaSV('${SV.id}')" 
   class="btn btn-warning">Sửa</button>
   </td>
   </tr>`;
    contentHTML += contentTr;
  });
  console.log("contentHTML: ", contentHTML);
  document.getElementById("tbodySinhVien").innerHTML = contentHTML;
};

function batLoading() {
  document.getElementById("loading").style.display = "flex";
}
function tatLoading() {
  document.getElementById("loading").style.display = "none";
}

// tren form
function layThongTinTuForm() {
  const maSV = document.getElementById("txtMaSV").value;
  const tenSV = document.getElementById("txtTenSV").value;
  const email = document.getElementById("txtEmail").value;
  const matKhau = document.getElementById("txtPass").value;
  const diemToan = document.getElementById("txtDiemToan").value;
  const diemLy = document.getElementById("txtDiemLy").value;
  const diemHoa = document.getElementById("txtDiemHoa").value;
  return new SinhVien(tenSV, email, matKhau, diemToan, diemLy, diemHoa, maSV);
}

// lay tren mockAPI
function showThongTinLenForm(SV) {
  document.getElementById("txtMaSV").value = SV.id;
  document.getElementById("txtTenSV").value = SV.name;
  document.getElementById("txtEmail").value = SV.email;
  document.getElementById("txtPass").value = SV.passW;
  document.getElementById("txtDiemToan").value = SV.math;
  document.getElementById("txtDiemLy").value = SV.physics;
  document.getElementById("txtDiemHoa").value = SV.chemistry;
}


